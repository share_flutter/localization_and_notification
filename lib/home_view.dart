import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_demo/contact_view.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    getToken();
    getNotification();
    super.initState();
  }

  getToken() async {
    await AwesomeNotifications().firebaseAppToken.then((value) {
      print("token $value");
    });
  }

  getNotification() {
    AwesomeNotifications().actionStream.listen((receivedNotification) {
      print("data receive: $receivedNotification");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("name").tr(),
            Text(
              "change_language",
              style: TextStyle(fontSize: 20),
            ).tr(),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    context.locale = Locale('km', 'KH');
                  });
                },
                child: Text("Khmer")),
            ElevatedButton(
                onPressed: () {
                  context.locale = Locale('en', 'US');
                },
                child: Text("English")),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => ContactView()));
                },
                child: Text("Go Contact"))
          ],
        ),
      ),
    );
  }
}
