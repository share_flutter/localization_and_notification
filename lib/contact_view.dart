import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ContactView extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<ContactView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("contact_us").tr(),
      ),
    );
  }
}
